import java.util.TreeSet;

public interface Service {
      void execute (TreeSet<Employee> employees);
}
