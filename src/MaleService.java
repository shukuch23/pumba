import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/*2)	Записать в файл "male.txt" всех мужчин от 20 до 40 лет, у которых ФИО начинается с одной и той же буквы.
	Например: Архипов Антон Андреевич.*/
public class MaleService implements Service {
      @Override
      public void execute(TreeSet<Employee> employees) {
            try {
                  BufferedWriter fileWritter = new BufferedWriter(
                   new FileWriter(new File("/home/admin/Programs/Code/Java/Study/Pumba/src/male.txt")));
                  List<Employee> buf = employees.stream()
                   .filter(s -> s.getAge() >= 20 && s.getAge() < 40 && s.getSex() == Sex.Male)
                   .collect(Collectors.toList());
                  boolean fl = false;
                  for (Employee e : buf) {
                        String[] s = e.getFullName().split(" ");
                        char target = s[0].charAt(0);
                        if (s[1].charAt(0) == target && s[2].charAt(0) == target) {
                              fileWritter.write(e.toString() + '\n');
                              fl = true;
                        }
                  }
                  if (!fl) {
                        fileWritter.write("Нет сотрудника, удовлетворяющего критерию\n");
                  }
                  fileWritter.close();
            } catch (IOException e) {
                  e.printStackTrace();
            }
      }
}
