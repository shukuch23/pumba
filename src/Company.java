import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Company {
      private final TreeSet<Employee> employees = new TreeSet<>(Comparator.comparingInt(Employee::getAge));

      public void addEmployee(Employee e) {
            employees.add(e);
      }

      public void Do(Service service) {
            service.execute(employees);
      }

      public List<Employee> searchByPosition(String pos) {
            return employees.stream()
             .filter(e -> e.getPosition().equals(pos))
             .collect(Collectors.toList());
      }
}
