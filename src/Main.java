import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Formatter;

public class Main {

      private static final String inputFile = "/home/admin/Programs/Code/Java/Study/Pumba/src/input.txt";

      public static void main(String[] args) throws IOException {

            BufferedReader fReader = new BufferedReader(new FileReader(inputFile));
            Company company = new Company();
            String buf = null;
            while ((buf = fReader.readLine()) != null) {
                  company.addEmployee(strToEmpl(buf));
            }
            fReader.close();

            company.Do(new FemaleService());
            company.Do(new MaleService());

//            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//            System.out.println("Введите искомую должность: ");
//            company.searchByPosition(reader.readLine().toLowerCase()).stream().forEach(System.out::println);
      }

      private static Employee strToEmpl(String str) {
            String[] s = str.split(" ");
            Formatter form = new Formatter().format("%s %s %s", s[0], s[1], s[2]);

            Employee employee = new Employee(
             form.toString(),
             Integer.parseInt(s[3]),
             s[4].charAt(0) == 'м' || s[4].charAt(0) == 'М' ? Sex.Male : Sex.Female,
             Float.parseFloat(s[5]),
             s[6].toLowerCase()
            );
            return employee;
      }
}
