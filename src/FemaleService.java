import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/*1)	Выписать всю информацию о женщинах старше 30-ти лет в файл "female.txt".
	+ записать туда минимум, максимум, среднее арифметическое их месячных зарплат.*/
public class FemaleService implements Service {
      @Override
      public void execute(TreeSet<Employee> employees) {
            try {
                  BufferedWriter fileWritter = new BufferedWriter(
                   new FileWriter(new File("/home/admin/Programs/Code/Java/Study/Pumba/src/female.txt")));
                  List<Employee> buf = employees.stream()
                   .filter(w -> w.getAge() > 30 && w.getSex() == Sex.Female)
                   .collect(Collectors.toList());
                  if (!buf.isEmpty()) {
                        for (Employee e : buf)
                              fileWritter.write(e.toString() + '\n');
                        Comparator<Employee> comp = Comparator.comparing(Employee::getSalary);
                        fileWritter.write("\nmin: " + buf.stream().min(comp).get().getSalary());
                        fileWritter.write("\nmax: " + buf.stream().max(comp).get().getSalary());
                        fileWritter.write("\naverage: " + buf.stream().mapToDouble(Employee::getSalary).average().getAsDouble());
                  } else {
                        fileWritter.write("Нет сотрудника, удовлетворяющего критерию\n");
                  }
                  fileWritter.close();
            } catch (IOException e) {
                  e.printStackTrace();
            }
      }
}
