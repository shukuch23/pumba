public class Employee {
      private String fullName = null;
      private Integer age = null;
      private Sex sex = null;
      private Float salary = null;
      private String position = null;

      private Employee() { }

      public Employee(String fullName, Integer age, Sex sex, Float salary, String position) {
            this.fullName = fullName;
            this.age = age;
            this.sex = sex;
            this.salary = salary;
            this.position = position;
      }

      public String getFullName() {
            return fullName;
      }

      public void setFullName(String fullName) {
            this.fullName = fullName;
      }

      public Integer getAge() {
            return age;
      }

      public void setAge(Integer age) {
            this.age = age;
      }

      public Sex getSex() {
            return sex;
      }

      public void setSex(Sex sex) {
            this.sex = sex;
      }

      public Float getSalary() {
            return salary;
      }

      public void setSalary(Float salary) {
            this.salary = salary;
      }

      public String getPosition() {
            return position;
      }

      public void setPosition(String position) {
            this.position = position;
      }

      @Override
      public String toString() {
            return "{ " +
             fullName + ' ' + age + ' '
             + sex + ' '
             + salary + ' '
             + position + " }";
      }
}
